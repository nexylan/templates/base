# Base

Basic project template.

## Requirements

- Docker

## New project setup

Get the download link of the latest version from the [releases page](-/releases).

```bash
wget https://gitlab.com/nexylan/templates/base/-/archive/master/base-master.tar.gz
tar xf base-master.tar.gz
rm base-master.tar.gz
mv base-master my-project
cd my-project
git init .
```

## Usage

Run:

```
make
```
